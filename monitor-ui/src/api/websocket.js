import Bus from './eventBus'

let ws = new WebSocket('ws://192.168.181.200:8888/websocket')

ws.onopen = function (e) {
  console.log('WebSocket连接成功    状态码：' + this.readyState)
}

ws.onmessage = function (e) {
  try {
    let jsonObj = JSON.parse(e.data)
    if (jsonObj.topic) {
      Bus.$emit(jsonObj.topic, jsonObj.message)
    }
  } catch (e) {
    console.log('其他消息')
  }
}

ws.onclose = function (e) {
  console.log('WebSocket连接关闭    状态码：' + this.readyState)
}

ws.onerror = function (e) {
  console.log('WebSocket连接发生错误   状态码：' + this.readyState)
}

export default ws
