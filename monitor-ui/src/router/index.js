import Vue from 'vue'
import VueRouter from 'vue-router'
import CommentMonitor from '@/views/CommentMonitor'
import VisitMonitor from '@/views/VisitMonitor'
import OrderMonitor from '@/views/OrderMonitor'
import ActionMonitor from "@/views/ActionMonitor";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/comment_monitor',
    name: 'Home',
    meta: {
      title:'业务监控系统-首页'
    }
  },
  {
    path: '/comment_monitor',
    name: 'CommentMonitor',
    component: CommentMonitor,
    meta: {
      keepAlive: true, // 需要被缓存
      title:'业务监控系统-商品评价监控'
    }
  },
  {
    path: '/visit_monitor',
    name: 'VisitMonitor',
    component: VisitMonitor,
    meta: {
      keepAlive: true, // 需要被缓存
      title:'业务监控系统-页面访问监控'
    }
  },
  {
    path: '/order_monitor',
    name: 'OrderMonitor',
    component: OrderMonitor,
    meta: {
      keepAlive: true, // 需要被缓存
      title:'业务监控系统-订单实时监控'
    }
  },
  {
    path: '/action_monitor',
    name: 'ActionMonitor',
    component: ActionMonitor,
    meta: {
      keepAlive: true, // 需要被缓存
      title:'业务监控系统-用户行为监控'
    }
  },
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  to.meta.title && (document.title = to.meta.title);
  next()
});

export default router
