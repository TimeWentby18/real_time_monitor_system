import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import * as echarts from 'echarts'  //引入echarts基础库
import 'echarts-gl';      //引入echarts地图库
import ws from './api/websocket'
import VueAnimateNumber from 'vue-animate-number'

Vue.use(ElementUI)
Vue.use(VueAnimateNumber)
Vue.config.productionTip = false
Vue.prototype.ws = ws

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
