import com.alibaba.fastjson.serializer.SerializerFeature
import com.alibaba.fastjson.{JSON, JSONObject}
import com.ververica.cdc.connectors.mysql.source.MySqlSource
import com.ververica.cdc.connectors.mysql.table.StartupOptions
import com.ververica.cdc.debezium.JsonDebeziumDeserializationSchema
import net.ipip.ipdb.City
import org.apache.flink.api.common.eventtime.WatermarkStrategy
import org.apache.flink.api.common.functions.FlatMapFunction
import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.cep.functions.PatternProcessFunction
import org.apache.flink.cep.nfa.aftermatch.AfterMatchSkipStrategy
import org.apache.flink.cep.scala.CEP
import org.apache.flink.cep.scala.pattern.Pattern
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.{ProcessWindowFunction, RichWindowFunction}
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector
import redis.clients.jedis.Jedis

import java.text.SimpleDateFormat
import java.util
import java.util.{Date, TimeZone}

case class UserVisitLog(ts: Long, siteID: String, userName: String, ip: String)

case class VisitCountResult(windowEnd: Long, siteID: String, countType: String, count: Long)

object VisitMonitor {
  def main(args: Array[String]): Unit = {
    // 从命令行中取出参数
    val parameters = ParameterTool.fromArgs(args)
    val windowTime = Integer.parseInt(parameters.get("w", "2")) // 默认值2
    val pvc = Integer.parseInt(parameters.get("pc", "26"))
    val pvt = Integer.parseInt(parameters.get("pt", "2"))
    val uvc = Integer.parseInt(parameters.get("uc", "5"))
    val uvt = Integer.parseInt(parameters.get("ut", "2"))
    val ipc = Integer.parseInt(parameters.get("ic", "5"))
    val ipt = Integer.parseInt(parameters.get("it", "2"))
    val ipAddress = parameters.get("ip", "localhost")
    // 定义MysqlCDC数据源
    val dataSource = MySqlSource.builder[String]()
      .hostname(ipAddress)
      .port(3306)
      .databaseList("log_db") // 设置捕获的数据库
      .tableList("log_db.visit_log") // 设置捕获的数据表
      .username("root")
      .password("root")
      .deserializer(new JsonDebeziumDeserializationSchema()) // 将SourceRecord转换成JSON字符串
      .startupOptions(StartupOptions.latest) // 从最新数据开始读取
      .build()
    // 创建执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    // 启动checkpoint以处理增量数据
    env.enableCheckpointing(3000).setParallelism(1)
    // 将原始数据转换为UserVisitLog对象
    val cdcDataStream = env.fromSource(dataSource, WatermarkStrategy.noWatermarks(), "MySql Source")
      .flatMap(new FlatMapFunction[String, UserVisitLog] {
        override def flatMap(in: String, collector: Collector[UserVisitLog]): Unit = {
          val jsonObj = JSON.parseObject(in).getJSONObject("after")
          if (jsonObj != null) {
            val ts = jsonObj.getLong("ts")
            val siteID = jsonObj.getString("website_id")
            val userName = jsonObj.getString("user_name")
            val ip = jsonObj.getString("ip")
            collector.collect(UserVisitLog(ts, siteID, userName, ip))
          }
        }
      })
      .assignAscendingTimestamps(_.ts) // 分配水位线

    // 利用滚动窗口实时统计各站点pv/uv/ip访问数
    val countResultStream = cdcDataStream.keyBy(_.siteID)
      .window(TumblingEventTimeWindows.of(Time.seconds(windowTime)))
      .apply(new RichWindowFunction[UserVisitLog, VisitCountResult, String, TimeWindow] {
        // 统计需要用到的三个键控状态
        private var pvCount: ValueState[Long] = _ // 记录当前pv访问数
        private var usernameSet: ValueState[Set[String]] = _ // 存放曾经访问过某站点的所有用户的用户名
        private var ipSet: ValueState[Set[String]] = _ // 存放曾经访问过某站点的所有IP
        private var time: ValueState[Long] = _ // 存放上一次统计的时间
        private var sdf: SimpleDateFormat = _

        override def open(parameters: Configuration): Unit = { // 获取状态
          pvCount = getRuntimeContext.getState(
            new ValueStateDescriptor[Long]("pv-count", createTypeInformation[Long])
          )
          usernameSet = getRuntimeContext.getState(
            new ValueStateDescriptor[Set[String]]("username-set", createTypeInformation[Set[String]])
          )
          ipSet = getRuntimeContext.getState(
            new ValueStateDescriptor[Set[String]]("ip-set", createTypeInformation[Set[String]])
          )
          time = getRuntimeContext.getState(
            new ValueStateDescriptor[Long]("time", createTypeInformation[Long])
          )
          sdf = new SimpleDateFormat("d")
          sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"))
        }

        override def apply(key: String,
                           window: TimeWindow,
                           elements: Iterable[UserVisitLog],
                           out: Collector[VisitCountResult]): Unit = {
          if (time.value() != 0) {  // 一天后清空状态，之前访问过的用户和IP重新统计
            val lastDay = Integer.parseInt(sdf.format(new Date(time.value())))
            val nowDay = Integer.parseInt(sdf.format(new Date(window.getEnd)))
            if (nowDay != lastDay) {
              usernameSet.clear()
              ipSet.clear()
            }
          }
          time.update(window.getEnd)
          if (usernameSet.value == null)
            usernameSet.update(Set[String]())
          if (ipSet.value == null)
            ipSet.update(Set[String]())
          for (e <- elements) {
            pvCount.update(pvCount.value + 1L)
            usernameSet.update(usernameSet.value + e.userName)
            ipSet.update(ipSet.value + e.ip)
          }
          out.collect(VisitCountResult(window.getEnd, key, "pv", pvCount.value))
          out.collect(VisitCountResult(window.getEnd, key, "uv", usernameSet.value.size))
          out.collect(VisitCountResult(window.getEnd, key, "ip", ipSet.value.size))
        }
      })
      .keyBy(_.windowEnd)
      .window(TumblingEventTimeWindows.of(Time.seconds(windowTime)))
      .process(new ProcessWindowFunction[VisitCountResult, String, Long, TimeWindow] {
        override def process(key: Long,
                             context: Context,
                             elements: Iterable[VisitCountResult],
                             out: Collector[String]): Unit = {
          val pvJsonObj = new JSONObject()
          val uvJsonObj = new JSONObject()
          val ipJsonObj = new JSONObject()
          for (e <- elements) {
            val info = new JSONObject()
            info.put("value", e.count)
            info.put("timestamp", e.windowEnd)
            e.countType match {
              case "pv" => pvJsonObj.put(e.siteID, info)
              case "uv" => uvJsonObj.put(e.siteID, info)
              case "ip" => ipJsonObj.put(e.siteID, info)
            }
          }
          val timestamp = elements.iterator.next().windowEnd
          for (i <- 1 to 8) { // 保证每个站点都有数据
            val siteID = "site" + i
            if (!pvJsonObj.containsKey(siteID)) {
              val pvInfo = new JSONObject()
              pvInfo.put("timestamp", timestamp)
              pvJsonObj.put(siteID, pvInfo)
            }
            if (!uvJsonObj.containsKey(siteID)) {
              val uvInfo = new JSONObject()
              uvInfo.put("timestamp", timestamp)
              uvJsonObj.put(siteID, uvInfo)
            }
            if (!ipJsonObj.containsKey(siteID)) {
              val ipInfo = new JSONObject()
              ipInfo.put("timestamp", timestamp)
              ipJsonObj.put(siteID, ipInfo)
            }
          }
          val jedis = new Jedis(ipAddress, 6379)
          jedis.auth("123456")
          val transaction = jedis.multi()
          val key1 = "visit:line:pv"
          val key2 = "visit:line:uv"
          val key3 = "visit:line:ip"
          val jsonStr1 = JSON.toJSONString(pvJsonObj, SerializerFeature.PrettyFormat)
          val jsonStr2 = JSON.toJSONString(uvJsonObj, SerializerFeature.PrettyFormat)
          val jsonStr3 = JSON.toJSONString(ipJsonObj, SerializerFeature.PrettyFormat)
          transaction.set(key1, jsonStr1)
          transaction.set(key2, jsonStr2)
          transaction.set(key3, jsonStr3)
          transaction.publish(key1, jsonStr1)
          transaction.publish(key2, jsonStr2)
          transaction.publish(key3, jsonStr3)
          transaction.exec()
          out.collect(jsonStr1)
          out.collect(jsonStr2)
          out.collect(jsonStr3)
        }
      }) // 将同一时刻不同站点的访问数据聚合起来发送到redis
//    countResultStream.print()

    // 使用CEP警告某时间内某站点访问数/某用户访问数/某IP访问数超过阈
    // 格式化时间
    val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"))
    // PV监控警告
    val pvAlertPattern = Pattern.begin[UserVisitLog]("pvStart", AfterMatchSkipStrategy.skipPastLastEvent())
      .where(_.siteID.nonEmpty).times(pvc).within(Time.seconds(pvt))
    val pvAlertPatternStream = CEP.pattern(cdcDataStream.keyBy(_.siteID), pvAlertPattern)
    val pvAlertResultStream = pvAlertPatternStream.process(
      (map: util.Map[String, util.List[UserVisitLog]],
       context: PatternProcessFunction.Context,
       collector: Collector[String]) => {
        val start = map.get("pvStart")
        val siteID = start.get(0).siteID
        val time = sdf.format(new Date(start.get(0).ts))
        val alertStr = "站点 " + siteID + " 在 " + time + " 时刻开始的" + pvt + "秒内的访问数达 " +
          pvc + " 次或以上，超过设定的阈值！"
        val jsonObj = new JSONObject()
        jsonObj.put("time", time)
        jsonObj.put("info", alertStr)
        val jedis = new Jedis(ipAddress, 6379)
        jedis.auth("123456")
        val transaction = jedis.multi()
        val key = "visit:alert:pv"
        val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
        transaction.set(key, jsonStr)
        transaction.publish(key, jsonStr)
        transaction.exec()
        collector.collect(alertStr)
      })
//    pvAlertResultStream.print()
    // 用户访问过于频繁警告
    val uvAlertPattern = Pattern.begin[UserVisitLog]("uvStart", AfterMatchSkipStrategy.skipPastLastEvent())
      .where(_.userName.nonEmpty).times(uvc).within(Time.seconds(uvt))
    val uvAlertPatternStream = CEP.pattern(cdcDataStream.keyBy(e => (e.siteID, e.userName)), uvAlertPattern)
    val uvAlertResultStream = uvAlertPatternStream.process(
      (map: util.Map[String, util.List[UserVisitLog]],
       context: PatternProcessFunction.Context,
       collector: Collector[String]) => {
        val start = map.get("uvStart")
        val siteID = start.get(0).siteID
        val userName = start.get(0).userName
        val time = sdf.format(new Date(start.get(0).ts))
        val alertStr = "用户 " + userName + " 在" + time + " 时刻开始的" + uvt + "秒内访问站点 " +
          siteID + " 达 " + uvc + " 次或以上，超过设定的阈值！"
        val jsonObj = new JSONObject()
        jsonObj.put("time", time)
        jsonObj.put("info", alertStr)
        val jedis = new Jedis(ipAddress, 6379)
        jedis.auth("123456")
        val transaction = jedis.multi()
        val key = "visit:alert:uv"
        val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
        transaction.set(key, jsonStr)
        transaction.publish(key, jsonStr)
        transaction.exec()
        collector.collect(alertStr)
      })
//    uvAlertResultStream.print()
    // ip访问过于频繁警告
    val ipAlertPattern = Pattern.begin[UserVisitLog]("ipStart", AfterMatchSkipStrategy.skipPastLastEvent())
      .where(_.ip.nonEmpty).times(ipc).within(Time.seconds(ipt))
    val ipAlertPatternStream = CEP.pattern(cdcDataStream.keyBy(e => (e.siteID, e.ip)), ipAlertPattern)
    val ipAlertResultStream = ipAlertPatternStream.process(
      (map: util.Map[String, util.List[UserVisitLog]],
       context: PatternProcessFunction.Context,
       collector: Collector[String]) => {
        val start = map.get("ipStart")
        val siteID = start.get(0).siteID
        val ip = start.get(0).ip
        val time = sdf.format(new Date(start.get(0).ts))
        // 读取IP数据库，查询该IP对应的城市名
        val cityDb = new City(this.getClass.getClassLoader.getResourceAsStream("ipipfree.ipdb"))
        var cityName = cityDb.findInfo(ip, "CN").getCityName
        if (cityName == null) cityName = "未知"
        val alertStr = "ip地址 " + ip + " 在" + time + " 时刻开始的" + ipt + "秒内访问站点 " +
          siteID + " 达 " + ipc + " 次或以上，超过设定的阈值！该IP来自：" + cityName
        val jsonObj = new JSONObject()
        jsonObj.put("time", time)
        jsonObj.put("info", alertStr)
        val jedis = new Jedis(ipAddress, 6379)
        jedis.auth("123456")
        val transaction = jedis.multi()
        val key = "visit:alert:ip"
        val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
        transaction.set(key, jsonStr)
        transaction.publish(key, jsonStr)
        transaction.exec()
        collector.collect(alertStr)
      })
//    ipAlertResultStream.print()

    env.execute("visit monitor")
  }
}
