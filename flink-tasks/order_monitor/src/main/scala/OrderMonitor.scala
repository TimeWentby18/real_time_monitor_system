import com.alibaba.fastjson.serializer.SerializerFeature
import com.alibaba.fastjson.{JSON, JSONObject}
import com.ververica.cdc.connectors.mysql.source.MySqlSource
import com.ververica.cdc.connectors.mysql.table.StartupOptions
import com.ververica.cdc.debezium.JsonDebeziumDeserializationSchema
import org.apache.flink.api.common.eventtime.WatermarkStrategy
import org.apache.flink.api.common.functions.FlatMapFunction
import org.apache.flink.api.common.state.{ListState, ListStateDescriptor}
import org.apache.flink.api.common.typeinfo.{TypeHint, TypeInformation}
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.cep.functions.PatternProcessFunction
import org.apache.flink.cep.nfa.aftermatch.AfterMatchSkipStrategy
import org.apache.flink.cep.scala.CEP
import org.apache.flink.cep.scala.pattern.Pattern
import org.apache.flink.runtime.state.{FunctionInitializationContext, FunctionSnapshotContext}
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.ProcessAllWindowFunction
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector
import redis.clients.jedis.Jedis

import java.text.SimpleDateFormat
import java.util
import java.util.{Date, TimeZone}
import scala.collection.JavaConverters.iterableAsScalaIterableConverter
import scala.collection.mutable

case class UserOrderLog(ts: Long, orderID: Long, orderType: String, userName: String, productID: String)

case class OrderCountResult(ts: Long, status: String, count: Long)

object OrderMonitor {
  def main(args: Array[String]): Unit = {
    // 从命令行中取出参数
    val parameters = ParameterTool.fromArgs(args)
    val windowTime = Integer.parseInt(parameters.get("w", "1")) // 默认值1
    val count = Integer.parseInt(parameters.get("c", "7"))
    val time = Integer.parseInt(parameters.get("t", "5"))
    val ipAddress = parameters.get("ip", "localhost")
    // MysqlCDC数据源
    val dataSource = MySqlSource.builder[String]()
      .hostname(ipAddress)
      .port(3306)
      .databaseList("log_db") // 设置捕获的数据库
      .tableList("log_db.order_log") // 设置捕获的数据表
      .username("root")
      .password("root")
      .deserializer(new JsonDebeziumDeserializationSchema()) // 将SourceRecord转换成JSON字符串
      .startupOptions(StartupOptions.latest) // 从最新数据开始读取
      .build()
    // 创建执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    // 启动checkpoint以处理增量数据
    env.enableCheckpointing(3000).setParallelism(1)
    // 将原始数据转换为UserOrderLog对象
    val cdcDataStream = env.fromSource(dataSource, WatermarkStrategy.noWatermarks(), "MySql Source")
      .flatMap(new FlatMapFunction[String, UserOrderLog] {
        override def flatMap(in: String, collector: Collector[UserOrderLog]): Unit = {
          val jsonObj = JSON.parseObject(in).getJSONObject("after")
          if (jsonObj != null) {
            val ts = jsonObj.getLong("ts")
            val orderID = jsonObj.getLong("order_id")
            val orderType = jsonObj.getString("type")
            val userName = jsonObj.getString("user_name")
            val productID = jsonObj.getString("product_id")
            collector.collect(UserOrderLog(ts, orderID, orderType, userName, productID))
          }
        }
      })
      .assignAscendingTimestamps(_.ts) // 分配水位线

    // 滚动窗口实时统计成功支付的订单数、过期订单数和失败订单数，用于前端实时显示
    val countResultStream = cdcDataStream.windowAll(TumblingEventTimeWindows.of(Time.seconds(windowTime)))
      .process(new OrderCountProcess(ipAddress))
//    countResultStream.print()

    // CEP实时监控刷单行为，并发出警告
    val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"))
    val sdAlertPattern = Pattern.begin[UserOrderLog]("sdStart", AfterMatchSkipStrategy.skipPastLastEvent())
      .where(e => "paid".equals(e.orderType)).times(count).within(Time.seconds(time))
    val sdAlertPatternStream = CEP.pattern(cdcDataStream.keyBy(_.productID), sdAlertPattern)
    val sdAlertResultStream = sdAlertPatternStream.process(
      (map: util.Map[String, util.List[UserOrderLog]],
       context: PatternProcessFunction.Context,
       collector: Collector[String]) => {
        val start = map.get("sdStart")
        val productID = start.get(0).productID
        val t = sdf.format(new Date(start.get(0).ts))
        val alertStr = "请注意！商品 " + productID + " 在 " + t + " 时刻开始的" + time + "秒内的成功支付订单数达 " +
          count + " 次或以上，可能存在刷单行为！"
        val jsonObj = new JSONObject()
        jsonObj.put("time", t)
        jsonObj.put("info", alertStr)
        val jedis = new Jedis(ipAddress, 6379)
        jedis.auth("123456")
        val transaction = jedis.multi()
        val key = "order:alert:sd"
        val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
        transaction.set(key, jsonStr)
        transaction.publish(key, jsonStr)
        transaction.exec()
        collector.collect(alertStr)
      })
//    sdAlertResultStream.print()

    env.execute("order monitor")
  }

  // 自定义带算子状态的全量窗口处理函数
  class OrderCountProcess(ipAddress: String)
    extends ProcessAllWindowFunction[UserOrderLog, OrderCountResult, TimeWindow]
      with CheckpointedFunction {
    // 使用列表算子状态增加可靠性
    @transient
    private var orderSetState: ListState[mutable.Set[UserOrderLog]] = _
    @transient
    private var successCountState: ListState[Long] = _
    @transient
    private var failedCountState: ListState[Long] = _
    @transient
    private var overdueCountState: ListState[Long] = _
    // 使用到的本地变量
    private val orderSet: mutable.Set[UserOrderLog] = mutable.Set() // 统计过程中临时存放订单日志
    private var successCount: Long = 0 // 成功支付的订单数
    private var failedCount: Long = 0 // 支付失败的订单数
    private var overdueCount: Long = 0 // 过期的订单数

    override def process(context: Context,
                         elements: Iterable[UserOrderLog],
                         out: Collector[OrderCountResult]): Unit = {
      // 建立jedis连接
      val jedis = new Jedis(ipAddress, 6379)
      jedis.auth("123456")
      val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
      sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"))
      for (e <- elements) {
        val findElem = orderSet.find(_.orderID == e.orderID)
        if ("created".equals(e.orderType)) { // 来一个created则将订单加入状态中
          orderSet += e
        } else if ("paid".equals(e.orderType)) { // 来一个paid则消去状态中的该订单，成功支付数+1
          if (findElem.isDefined) {
            orderSet -= findElem.get
            successCount += 1
          }
        } else if ("failed".equals(e.orderType)) { // 来一个failed则消去状态中的该订单，失败订单数+1
          if (findElem.isDefined) {
            orderSet -= findElem.get
            failedCount += 1
            val transaction = jedis.multi() // 创建一个redis事务，发送失败订单警告
            val key = "order:alert:failed"
            val time = sdf.format(new Date(e.ts))
            val orderID = e.orderID
            val userName = e.userName
            val productID = e.productID
            val alertStr = "请注意！在 " + time + " 时刻有一个失败订单，订单号为 " + orderID + " ，涉及商品为：" +
              productID + " ，涉及用户为：" + userName
            val jsonObj = new JSONObject() // 包装成json对象
            jsonObj.put("time", time)
            jsonObj.put("info", alertStr)
            val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
            transaction.set(key, jsonStr)
            transaction.publish(key, jsonStr)
            transaction.exec()
          }
        }
      }
      if (orderSet.nonEmpty) { // 处理完所有元素后检查状态中是否剩余有未消去的订单
        for (e <- orderSet) {
          if (e.ts < context.window.getEnd - 5000) { // 若有，则检查其时间戳是否已经过期，过期则消去状态中的该订单，过期订单数+1
            orderSet -= e
            overdueCount += 1
            val transaction = jedis.multi() // 创建一个redis事务，发送过期订单警告
            val key = "order:alert:overdue"
            val time = sdf.format(new Date(e.ts))
            val orderID = e.orderID
            val userName = e.userName
            val productID = e.productID
            val alertStr = "请注意！创建于 " + time + " 时刻的订单 " + orderID + " 已过期，涉及商品为：" +
              productID + "，涉及用户为：" + userName
            val jsonObj = new JSONObject()
            jsonObj.put("time", time)
            jsonObj.put("info", alertStr)
            val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
            transaction.set(key, jsonStr)
            transaction.publish(key, jsonStr)
            transaction.exec()
          }
        }
      }
      // 创建一个redis事务，发送计数信息
      val transaction = jedis.multi()
      val key = "order:value"
      val jsonObj = new JSONObject()
      jsonObj.put("success", successCount)
      jsonObj.put("failed", failedCount)
      jsonObj.put("overdue", overdueCount)
      val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
      transaction.set(key, jsonStr)
      transaction.publish(key, jsonStr)
      transaction.exec()

      out.collect(OrderCountResult(context.window.getEnd, "success", successCount))
      out.collect(OrderCountResult(context.window.getEnd, "failed", failedCount))
      out.collect(OrderCountResult(context.window.getEnd, "overdue", overdueCount))
    }

    override def snapshotState(context: FunctionSnapshotContext): Unit = { // 对数据进行快照，存储到列表状态中
      orderSetState.clear()
      orderSetState.add(orderSet)
      successCountState.clear()
      successCountState.add(successCount)
      failedCountState.clear()
      failedCountState.add(failedCount)
      overdueCountState.clear()
      overdueCountState.add(overdueCount)
    }

    override def initializeState(context: FunctionInitializationContext): Unit = { // 初始化状态
      orderSetState = context.getOperatorStateStore.getListState(
        new ListStateDescriptor[mutable.Set[UserOrderLog]](
          "order-set-list",
          TypeInformation.of(new TypeHint[mutable.Set[UserOrderLog]] {})))
      successCountState = context.getOperatorStateStore.getListState(
        new ListStateDescriptor[Long](
          "success-count-list",
          TypeInformation.of(new TypeHint[Long] {})))
      failedCountState = context.getOperatorStateStore.getListState(
        new ListStateDescriptor[Long](
          "failed-count-list",
          TypeInformation.of(new TypeHint[Long] {})))
      overdueCountState = context.getOperatorStateStore.getListState(
        new ListStateDescriptor[Long](
          "overdue-count-list",
          TypeInformation.of(new TypeHint[Long] {})))

      if (context.isRestored) { // 从列表状态中恢复数据
        for (element <- orderSetState.get().asScala) {
          orderSet ++= element
        }
        for (element <- successCountState.get().asScala) {
          successCount += element
        }
        for (element <- failedCountState.get().asScala) {
          failedCount += element
        }
        for (element <- overdueCountState.get().asScala) {
          overdueCount += element
        }
      }
    }
  }
}
