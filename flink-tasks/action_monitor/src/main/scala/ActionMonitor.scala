import com.alibaba.fastjson.serializer.SerializerFeature
import com.alibaba.fastjson.{JSON, JSONArray, JSONObject}
import com.ververica.cdc.connectors.mysql.source.MySqlSource
import com.ververica.cdc.connectors.mysql.table.StartupOptions
import com.ververica.cdc.debezium.JsonDebeziumDeserializationSchema
import net.ipip.ipdb.City
import org.apache.flink.api.common.eventtime.WatermarkStrategy
import org.apache.flink.api.common.functions.FlatMapFunction
import org.apache.flink.api.common.state.{ListState, ListStateDescriptor}
import org.apache.flink.api.common.typeinfo.{TypeHint, TypeInformation}
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.cep.functions.PatternProcessFunction
import org.apache.flink.cep.nfa.aftermatch.AfterMatchSkipStrategy
import org.apache.flink.cep.scala.CEP
import org.apache.flink.cep.scala.pattern.Pattern
import org.apache.flink.runtime.state.{FunctionInitializationContext, FunctionSnapshotContext}
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.ProcessAllWindowFunction
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector
import redis.clients.jedis.Jedis

import java.text.SimpleDateFormat
import java.util
import java.util.{Date, TimeZone}
import scala.collection.JavaConverters.iterableAsScalaIterableConverter
import scala.collection.mutable

case class UserActionLog(ts: Long, ip: String, userName: String, action: String, status: String)

object ActionMonitor {
  def main(args: Array[String]): Unit = {
    // 从命令行中取出参数
    val parameters = ParameterTool.fromArgs(args)
    val onlineRefreshTime = Integer.parseInt(parameters.get("ot", "5")) // 默认值5
    val loginCount = Integer.parseInt(parameters.get("lc", "4"))
    val loginTime = Integer.parseInt(parameters.get("lt", "5"))
    val registerCount = Integer.parseInt(parameters.get("rc", "14"))
    val registerTime = Integer.parseInt(parameters.get("rt", "5"))
    val ipAddress = parameters.get("ip", "localhost")
    // 定义MysqlCDC数据源
    val dataSource = MySqlSource.builder[String]()
      .hostname(ipAddress)
      .port(3306)
      .databaseList("log_db") // 设置捕获的数据库
      .tableList("log_db.action_log") // 设置捕获的数据表
      .username("root")
      .password("root")
      .deserializer(new JsonDebeziumDeserializationSchema()) // 将SourceRecord转换成JSON字符串
      .startupOptions(StartupOptions.latest) // 从最新数据开始读取
      .build()
    // 创建执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    // 启动checkpoint以处理增量数据
    env.enableCheckpointing(3000).setParallelism(1)
    // 将原始数据转换为UserActionLog对象
    val cdcDataStream = env.fromSource(dataSource, WatermarkStrategy.noWatermarks(), "MySql Source")
      .flatMap(new FlatMapFunction[String, UserActionLog] {
        override def flatMap(in: String, collector: Collector[UserActionLog]): Unit = {
          val jsonObj = JSON.parseObject(in).getJSONObject("after")
          if (jsonObj != null) {
            val ts = jsonObj.getLong("ts")
            val ip = jsonObj.getString("ip")
            val userName = jsonObj.getString("user_name")
            val action = jsonObj.getString("action")
            val status = jsonObj.getString("status")
            collector.collect(UserActionLog(ts, ip, userName, action, status))
          }
        }
      })
      .assignAscendingTimestamps(_.ts) // 分配水位线

    // 滚动窗口实时统计各城市在线人数，用于前端实时显示
    val onlineCountResult = cdcDataStream.windowAll(TumblingEventTimeWindows.of(Time.seconds(onlineRefreshTime)))
      .process(new OnlineCountProcess(ipAddress))
//    onlineCountResult.print()

    // CEP监控登录注册异常并发出相应的警告
    // 连续登录失败
    val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"))
    val loginFailedAlertPattern = Pattern.begin[UserActionLog]("loginFailedStart", AfterMatchSkipStrategy.skipPastLastEvent())
      .where(e => "login".equals(e.action) && "failed".equals(e.status)).times(loginCount).consecutive().within(Time.seconds(loginTime))
    val loginFailedAlertPatternStream = CEP.pattern(cdcDataStream.keyBy(_.userName), loginFailedAlertPattern)
    val loginFailedAlertResultStream = loginFailedAlertPatternStream.process(
      (map: util.Map[String, util.List[UserActionLog]],
       context: PatternProcessFunction.Context,
       collector: Collector[String]) => {
        val start = map.get("loginFailedStart")
        val ip = start.get(0).ip
        val userName = start.get(0).userName
        val time = sdf.format(new Date(start.get(0).ts))
        // 读取IP数据库，查询该IP对应的城市名
        val cityDb = new City(this.getClass.getClassLoader.getResourceAsStream("ipipfree.ipdb"))
        var cityName = cityDb.findInfo(ip, "CN").getCityName
        if (cityName == null) cityName = "未知"
        val alertStr = "警告：用户 " + userName + " 在 " + time + " 时刻开始的" + loginTime +
          "秒内连续登录失败达 " + loginCount + " 次，超过设定的阈值！登录IP来自：" + cityName
        val jsonObj = new JSONObject()
        jsonObj.put("title", "连续登录失败警告")
        jsonObj.put("type", "warning")
        jsonObj.put("description", alertStr)
        val jedis = new Jedis(ipAddress, 6379)
        jedis.auth("123456")
        val transaction = jedis.multi()
        val key = "action:alert:login"
        val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
        transaction.set(key, jsonStr)
        transaction.publish(key, jsonStr)
        transaction.exec()
        collector.collect(alertStr)
      })
    // loginFailedAlertResultStream.print()

    // 注册过于频繁
    val registerAlertPattern = Pattern.begin[UserActionLog]("registerStart", AfterMatchSkipStrategy.skipPastLastEvent())
      .where(e => "register".equals(e.action) && "success".equals(e.status)).times(registerCount).within(Time.seconds(registerTime))
    val registerAlertPatternStream = CEP.pattern(cdcDataStream, registerAlertPattern)
    val registerAlertResultStream = registerAlertPatternStream.process(
      (map: util.Map[String, util.List[UserActionLog]],
       context: PatternProcessFunction.Context,
       collector: Collector[String]) => {
        val start = map.get("registerStart")
        val time = sdf.format(new Date(start.get(0).ts))
        val alertStr = "警告：在 " + time + " 时刻开始的" + registerTime + "秒内注册成功的用户达 " +
          registerCount + " 个或以上，超过设定的阈值！"
        val jsonObj = new JSONObject()
        jsonObj.put("title", "注册过于频繁警告")
        jsonObj.put("type", "info")
        jsonObj.put("description", alertStr)
        val jedis = new Jedis(ipAddress, 6379)
        jedis.auth("123456")
        val transaction = jedis.multi()
        val key = "action:alert:register"
        val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
        transaction.set(key, jsonStr)
        transaction.publish(key, jsonStr)
        transaction.exec()
        collector.collect(alertStr)
      })
    // registerAlertResultStream.print()

    env.execute("action monitor")
  }

  // 自定义带算子状态的全量窗口处理函数
  class OnlineCountProcess(ipAddress: String)
    extends ProcessAllWindowFunction[UserActionLog, String, TimeWindow]
      with CheckpointedFunction {
    // 使用算子状态提高可靠性
    @transient
    private var onlineUserMapState: ListState[mutable.Map[String, String]] = _
    @transient
    private var onlineCityMapState: ListState[mutable.Map[String, Long]] = _
    // 使用到的本地变量，一个用于记录当前已登录用户及其登录城市的映射，另一个用来记录每个城市当前的在线人数
    val onlineUserMap: mutable.Map[String, String] = mutable.Map()
    var onlineCityMap: mutable.Map[String, Long] = mutable.Map()

    override def process(context: Context,
                         elements: Iterable[UserActionLog],
                         out: Collector[String]): Unit = {
      // 遍历每一个元素
      for (e <- elements) {
        if ("login".equals(e.action) && "success".equals(e.status)) { // 若为登录成功的记录
          val username = e.userName
          if (!onlineUserMap.keys.exists(_.equals(username))) { // 若该用户不在已在线用户名单中
            val cityDb = new City(this.getClass.getClassLoader.getResourceAsStream("ipipfree.ipdb"))
            val cityName = cityDb.findInfo(e.ip, "CN").getCityName // 获取ip对应的城市名称
            if (cityName != null && cityName.nonEmpty && !"".equals(cityName)) {
              onlineUserMap(username) = cityName // 添加到在线用户名单中
              onlineCityMap(cityName) = onlineCityMap.getOrElse(cityName, 0L) + 1L // 该城市在线人数加一
            }
          }
        } else if ("quit".equals(e.action) && "success".equals(e.status)) { // 若为用户退出操作
          val username = e.userName
          if (onlineUserMap.keys.exists(_.equals(username))) { // 若该用户在已在线用户名单中
            val cityName = onlineUserMap(username) // 获取ip对应的城市名称
            onlineUserMap.remove(username) // 从在线用户名单中移除该用户
            if (onlineCityMap(cityName) <= 1) { // 该城市在线人数减一
              onlineCityMap.remove(cityName)
            } else {
              onlineCityMap(cityName) -= 1L
            }
          }
        }
      }
      val jsonArray = new JSONArray()
      for (e <- onlineCityMap) {
        val jsonObj = new JSONObject()
        jsonObj.put("name", e._1)
        jsonObj.put("value", e._2)
        jsonArray.add(jsonObj)
      }
      val jsonArrayStr = jsonArray.toJSONString
      val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
      sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"))
      val time = sdf.format(new Date(context.window.getEnd))
      val jsonObj = new JSONObject()
      jsonObj.put("data", jsonArrayStr)
      jsonObj.put("time", time)
      // 建立jedis连接
      val jedis = new Jedis(ipAddress, 6379)
      jedis.auth("123456")
      val transaction = jedis.multi() // 创建一个redis事务，发送失败订单警告
      val key = "action:map:online"
      val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
      transaction.set(key, jsonStr)
      transaction.publish(key, jsonStr)
      transaction.exec()
      out.collect(jsonStr)
    }

    override def snapshotState(context: FunctionSnapshotContext): Unit = { // 对数据进行快照，存储到列表状态中
      onlineUserMapState.clear()
      onlineUserMapState.add(onlineUserMap)
      onlineCityMapState.clear()
      onlineCityMapState.add(onlineCityMap)
    }

    override def initializeState(context: FunctionInitializationContext): Unit = { // 初始化状态
      onlineUserMapState = context.getOperatorStateStore.getListState(
        new ListStateDescriptor[mutable.Map[String, String]](
          "user-map-list",
          TypeInformation.of(new TypeHint[mutable.Map[String, String]] {})))
      onlineCityMapState = context.getOperatorStateStore.getListState(
        new ListStateDescriptor[mutable.Map[String, Long]](
          "city-map-list",
          TypeInformation.of(new TypeHint[mutable.Map[String, Long]] {})))
      if (context.isRestored) {  // 从列表状态中恢复数据
        for (element <- onlineUserMapState.get().asScala) {
          onlineUserMap ++= element
        }
        for (element <- onlineCityMapState.get().asScala) {
          onlineCityMap ++= element.map(t => t._1 -> (t._2 + onlineCityMap.getOrElse(t._1, 0L)))
        }
      }
    }
  }
}
