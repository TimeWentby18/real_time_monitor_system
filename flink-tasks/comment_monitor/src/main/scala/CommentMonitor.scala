import com.alibaba.fastjson.serializer.SerializerFeature
import com.alibaba.fastjson.{JSON, JSONObject}
import com.ververica.cdc.connectors.mysql.source.MySqlSource
import com.ververica.cdc.connectors.mysql.table.StartupOptions
import com.ververica.cdc.debezium.JsonDebeziumDeserializationSchema
import org.apache.flink.api.common.eventtime.WatermarkStrategy
import org.apache.flink.api.common.functions.{AggregateFunction, FlatMapFunction}
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.cep.functions.PatternProcessFunction
import org.apache.flink.cep.nfa.aftermatch.AfterMatchSkipStrategy
import org.apache.flink.cep.scala.CEP
import org.apache.flink.cep.scala.pattern.Pattern
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.{ProcessWindowFunction, WindowFunction}
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector
import redis.clients.jedis.Jedis

import java.text.SimpleDateFormat
import java.util
import java.util.{Date, TimeZone}

case class ProductCommentLog(ts: Long, productID: String, userName: String, point: Int)

case class CommentCountResult(ts: Long, productID: String, commentType: Int, count: Long)

object CommentMonitor {
  def main(args: Array[String]): Unit = {
    // 从命令行中取出参数
    val parameters = ParameterTool.fromArgs(args)
    val windowTime = Integer.parseInt(parameters.get("w", "5")) // 默认值5
    val nc = Integer.parseInt(parameters.get("nc", "10"))
    val nt = Integer.parseInt(parameters.get("nt", "5"))
    val pc = Integer.parseInt(parameters.get("pc", "10"))
    val pt = Integer.parseInt(parameters.get("pt", "5"))
    val ipAddress = parameters.get("ip", "localhost")
    // 定义MysqlCDC数据源
    val dataSource = MySqlSource.builder[String]()
      .hostname(ipAddress)
      .port(3306)
      .databaseList("log_db") // 设置捕获的数据库
      .tableList("log_db.comment_log") // 设置捕获的数据表
      .username("root")
      .password("root")
      .deserializer(new JsonDebeziumDeserializationSchema()) // 将SourceRecord转换成JSON字符串
      .startupOptions(StartupOptions.latest) // 从最新数据开始读取
      .build()
    // 创建执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    // 启动checkpoint以处理增量数据
    env.enableCheckpointing(3000).setParallelism(1)
    // 将原始数据转换为ProductCommentLog对象
    val cdcDataStream = env.fromSource(dataSource, WatermarkStrategy.noWatermarks(), "MySql Source")
      .flatMap(new FlatMapFunction[String, ProductCommentLog] {
        override def flatMap(in: String, collector: Collector[ProductCommentLog]): Unit = {
          val jsonObj = JSON.parseObject(in).getJSONObject("after")
          if (jsonObj != null) {
            val ts = jsonObj.getLong("ts")
            val productID = "商品" + jsonObj.getString("product_id")
            val userName = jsonObj.getString("user_name")
            val point = jsonObj.getIntValue("point")
            collector.collect(ProductCommentLog(ts, productID, userName, point))
          }
        }
      })
      .assignAscendingTimestamps(_.ts) // 分配水位线
      .keyBy(_.productID)

    // 格式化时间
    val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"))
    // 利用滚动窗口实时统计各商品近期接收到的好评/差评数量
    // 1分表示差评，5分表示好评
    val countResultStream = cdcDataStream
      .window(TumblingEventTimeWindows.of(Time.seconds(windowTime)))
      .aggregate(new AggregateFunction[ProductCommentLog, (Long, Long), (Long, Long)] {
        override def createAccumulator(): (Long, Long) = (0L, 0L)

        override def add(in: ProductCommentLog, acc: (Long, Long)): (Long, Long) = {
          if (in.point == 1) (acc._1 + 1, acc._2)
          else if (in.point == 5) (acc._1, acc._2 + 1)
          else (acc._1, acc._2)
        }

        override def getResult(acc: (Long, Long)): (Long, Long) = acc

        override def merge(acc1: (Long, Long), acc2: (Long, Long)): (Long, Long) = {
          (acc1._1 + acc2._1, acc1._2 + acc2._2)
        }
      },
        new WindowFunction[(Long, Long), CommentCountResult, String, TimeWindow] {
          override def apply(key: String,
                             window: TimeWindow,
                             input: Iterable[(Long, Long)],
                             out: Collector[CommentCountResult]): Unit = {
            val count = input.iterator.next()
            // 0表示差评，1表示好评
            out.collect(CommentCountResult(window.getEnd, key, 0, count._1))
            out.collect(CommentCountResult(window.getEnd, key, 1, count._2))
          }
        }) // 结合增量聚合函数和全量聚合函数对数据进行处理
      .keyBy(_.ts)
      .window(TumblingEventTimeWindows.of(Time.seconds(windowTime)))
      .process(new ProcessWindowFunction[CommentCountResult, String, Long, TimeWindow] {
        override def process(key: Long,
                             context: Context,
                             elements: Iterable[CommentCountResult],
                             out: Collector[String]): Unit = {
          val negative = new JSONObject()
          val positive = new JSONObject()
          for (e <- elements) {
            if (e.commentType == 0) {
              negative.put(e.productID, e.count)
            } else {
              positive.put(e.productID, e.count)
            }
            out.collect(e.toString)
          }
          val time = sdf.format(new Date(context.window.getEnd))
          val jsonObjNeg = new JSONObject()
          jsonObjNeg.put("data", negative)
          jsonObjNeg.put("time", time)
          val jsonObjPos = new JSONObject()
          jsonObjPos.put("data", positive)
          jsonObjPos.put("time", time)
          val jedis = new Jedis(ipAddress, 6379) // 创建redis连接
          jedis.auth("123456") // 验证密码
          val transaction = jedis.multi() // 创建redis事务
          val key1 = "comment:bar:negative"
          val key2 = "comment:bar:positive"
          val jsonStr1 = JSON.toJSONString(jsonObjNeg, SerializerFeature.PrettyFormat)
          val jsonStr2 = JSON.toJSONString(jsonObjPos, SerializerFeature.PrettyFormat)
          transaction.set(key1, jsonStr1)
          transaction.set(key2, jsonStr2)
          transaction.publish(key1, jsonStr1) // 发布消息到对应key/topic
          transaction.publish(key2, jsonStr2)
          transaction.exec() // 执行事务
        }
      }) // 将同一时刻不同商品的评价数聚合起来发送到redis
//    countResultStream.print()

    // 利用CEP监控恶意差评现象并发送警告
    val negativeCommentPattern = Pattern.begin[ProductCommentLog]("ngStart", AfterMatchSkipStrategy.skipPastLastEvent())
      .where(_.point == 1).times(nc).within(Time.seconds(nt)) // AfterMatchSkipStrategy策略跳过已经匹配的项，减少警告的频率
    val negativeCommentStream = CEP.pattern(cdcDataStream, negativeCommentPattern)
    val negativeResultStream = negativeCommentStream.process(
      (map: util.Map[String, util.List[ProductCommentLog]],
       context: PatternProcessFunction.Context,
       out: Collector[String]) => {
        val start = map.get("ngStart")
        val id = start.get(0).productID
        val time = sdf.format(new Date(start.get(0).ts))
        val alertStr = id + " 在 " + time + " 时刻起的 " + nt + "秒 时间内收到差评数达 " + nc + " 次或以上，超过设定的阈值！"
        val jsonObj = new JSONObject()
        jsonObj.put("time", time)
        jsonObj.put("info", alertStr)
        val jedis = new Jedis(ipAddress, 6379)
        jedis.auth("123456")
        val transaction = jedis.multi()
        val key = "comment:alert:negative"
        val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
        transaction.set(key, jsonStr)
        transaction.publish(key, jsonStr)
        transaction.exec()
        out.collect(alertStr)
      })
//    negativeResultStream.print()

    // 利用CEP监控刷好评现象并发送警告
    val positiveCommentPattern = Pattern.begin[ProductCommentLog]("psStart", AfterMatchSkipStrategy.skipPastLastEvent())
      .where(_.point == 5).times(pc).within(Time.seconds(pt)) // AfterMatchSkipStrategy策略跳过已经匹配的项，减少警告的频率
    val positiveCommentStream = CEP.pattern(cdcDataStream, positiveCommentPattern)
    val positiveResultStream = positiveCommentStream.process(
      (map: util.Map[String, util.List[ProductCommentLog]],
       context: PatternProcessFunction.Context,
       out: Collector[String]) => {
        val start = map.get("psStart")
        val id = start.get(0).productID
        val time = sdf.format(new Date(start.get(0).ts))
        val alertStr = id + " 在 " + time + " 时刻起的 " + pt + "秒 时间内收到好评数达 " + pc + " 次或以上，超过设定的阈值！"
        val jsonObj = new JSONObject()
        jsonObj.put("time", time)
        jsonObj.put("info", alertStr)
        val jedis = new Jedis(ipAddress, 6379)
        jedis.auth("123456")
        val transaction = jedis.multi()
        val key = "comment:alert:positive"
        val jsonStr = JSON.toJSONString(jsonObj, SerializerFeature.PrettyFormat)
        transaction.set(key, jsonStr)
        transaction.publish(key, jsonStr)
        transaction.exec()
        out.collect(alertStr)
      })
//    positiveResultStream.print()

    env.execute("comment monitor")
  }

}
