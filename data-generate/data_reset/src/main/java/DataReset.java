import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DataReset {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        // 加载驱动程序
        Class.forName("com.mysql.jdbc.Driver");
        // 从命令行参数中获取数据库ip、用户名和密码
        String ip = args[0];
        String username = args[1];
        String pwd = args[2];
        // 创建连接
        String url = "jdbc:mysql://" + ip + ":3306/log_db?useUnicode=true&characterEncoding=utf-8&useSSL=false";
        Connection conn = DriverManager.getConnection(url, username, pwd);
        Statement stmt = conn.createStatement();
        String sql1 = "DELETE FROM comment_log";
        String sql2 = "DELETE FROM visit_log";
        String sql3 = "DELETE FROM order_log";
        String sql4 = "DELETE FROM action_log";
        stmt.executeUpdate(sql1);
        stmt.executeUpdate(sql2);
        stmt.executeUpdate(sql3);
        stmt.executeUpdate(sql4);
        stmt.close();
        conn.close();
    }
}
