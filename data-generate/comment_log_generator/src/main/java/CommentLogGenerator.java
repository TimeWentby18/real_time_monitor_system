import java.sql.*;
import java.util.Random;

public class CommentLogGenerator {
    public static void main(String[] args) throws InterruptedException, ClassNotFoundException, SQLException {
        // 加载驱动程序
        Class.forName("com.mysql.jdbc.Driver");
        // 从命令行参数中获取数据库ip、用户名和密码
        String ip = args[0];
        String username = args[1];
        String pwd = args[2];
        // 创建连接
        String url = "jdbc:mysql://" + ip + ":3306/log_db?useUnicode=true&characterEncoding=utf-8&useSSL=false";
        Connection conn = DriverManager.getConnection(url, username, pwd);
        // 创建prepareStatement执行动态sql
        String insert = "insert into comment_log(ts, product_id, user_name, point) values (?, ?, ?, ?)";
        PreparedStatement state = conn.prepareStatement(insert);
        // 循环产生随机数据，并插入数据库
        // ts时间戳+productID商品ID+userName用户名+point评分(1~5分)
        double probability = 0.0; // 评分概率，用来控制用户的评分倾向
        boolean isPositive = true; // 当前用户评分倾向正面或反面
        while (true) {
            if (probability >= 1.0) {
                isPositive = !isPositive;
                probability = 0.0;
            }
            probability += 0.05;
            try {
                for (int j = 0; j < 5; j++) {
                    state.setLong(1, getTimeStamp());
                    state.setString(2, getProductID());
                    state.setString(3, getUserName());
                    state.setInt(4, getPoint(probability, isPositive));
                    state.addBatch();
                }
                state.executeBatch();
                state.clearBatch();
            } catch (SQLException e) {
                e.printStackTrace();
                //关闭连接
                try {
                    state.close();
                    conn.close();
                    break;
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
//            System.out.println("已批量提交sql请求！");
            Thread.sleep(200);
        }
    }

    // 获取当前时间戳
    static private Long getTimeStamp() {
        return System.currentTimeMillis();
    }

    // 随机生成商品ID
    static private String getProductID() {
        String[] arr = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
        Random random = new Random();
        return arr[random.nextInt(10)];
    }

    // 随机生成用户名
    static private String getUserName() {
        Random random = new Random();
        int id = random.nextInt(999) + 1;
        return "user" + id;
    }

    // 随机生成用户评分
    static private int getPoint(double probability, boolean isPositive) {
        Random random = new Random();
        if (isPositive) {
            if (random.nextDouble() < probability) return 5;
        } else {
            if (random.nextDouble() < probability) return 1;
        }
        int[] arr = new int[]{1, 2, 3, 4, 5};
        return arr[random.nextInt(5)];
    }
}
