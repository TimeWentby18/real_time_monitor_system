import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;

public class ActionLogGenerator {
    public static void main(String[] args) throws InterruptedException, ClassNotFoundException, SQLException {
        // 加载驱动程序
        Class.forName("com.mysql.jdbc.Driver");
        // 从命令行参数中获取数据库ip、用户名和密码
        String ip = args[0];
        String username = args[1];
        String pwd = args[2];
        // 创建连接
        String url = "jdbc:mysql://" + ip + ":3306/log_db?useUnicode=true&characterEncoding=utf-8&useSSL=false";
        Connection conn = DriverManager.getConnection(url, username, pwd);
        // 创建prepareStatement执行动态sql
        String insert = "insert into action_log(ts, ip, user_name, action, status) values (?, ?, ?, ?, ?)";
        PreparedStatement state = conn.prepareStatement(insert);
        // 循环产生随机数据，并插入数据库
        // ts时间戳+ip地址+userName用户名+action用户行为(login/register/quit)+status操作状态(success/failed)
        double ratio = 3.0; // 用于控制status成功或失败的概率
        Random random = new Random();
        String[] arr;
        while (true) {
            if (ratio < 1.0) {
                ratio = 3.0;
            }
            try {
                // 用户登录日志(警告模块)
                state.setLong(1, getTimeStamp());
                arr = getIPAndUserName();
                state.setString(2, arr[0]);
                state.setString(3, arr[1]);
                state.setString(4, "login");
                if (random.nextDouble() < 0.3 * ratio) {
                    state.setString(5, "success");
                } else {
                    state.setString(5, "failed");
                }
                state.addBatch();
                // 用户注册日志(警告模块)
                state.setLong(1, getTimeStamp());
                state.setString(2, getRandomIp());
                state.setString(3, getRandomUserName());
                state.setString(4, "register");
                if (random.nextDouble() < 0.3 * ratio) {
                    state.setString(5, "failed");
                } else {
                    state.setString(5, "success");
                }
                state.addBatch();
                // 用户登录日志(在线人数模块)
                state.setLong(1, getTimeStamp());
                state.setString(2, getRandomIp());
                state.setString(3, getRandomUserName());
                state.setString(4, "login");
                state.setString(5, "success");
                state.addBatch();
                // 用户退出日志(在线人数模块)
                if (random.nextDouble() < 0.4) {
                    state.setLong(1, getTimeStamp());
                    state.setString(2, getRandomIp());
                    state.setString(3, getRandomUserName());
                    state.setString(4, "quit");
                    state.setString(5, "success");
                    state.addBatch();
                }
                state.executeBatch();
                state.clearBatch();
            } catch (SQLException e) {
                e.printStackTrace();
                //关闭连接
                try {
                    state.close();
                    conn.close();
                    break;
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            ratio -= random.nextDouble() / 5;
            Thread.sleep(200);
//            System.out.println("已批量提交sql请求！");
        }
    }

    // 获取当前时间戳
    static private Long getTimeStamp() {
        return System.currentTimeMillis();
    }

    // 随机生成用户名及其对应的IP地址
    static private String[] getIPAndUserName() {
        Random random = new Random();
        int randomNum = random.nextInt(9) + 1;
        String ip;
        if (randomNum < 4) {
            ip = "58.249.115." + randomNum;
        } else if (randomNum < 7) {
            ip = "180.160.12." + randomNum;
        } else {
            ip = "117.112.82." + randomNum;
        }
        String username = "user" + randomNum;
        String[] arr = new String[2];
        arr[0] = ip;
        arr[1] = username;
        return arr;
    }

    // 随机生成国内IP地址
    public static String getRandomIp() {
        // ip范围
        int[][] range = {{607649792, 608174079}, // 36.56.0.0-36.63.255.255
                {1038614528, 1039007743}, // 61.232.0.0-61.237.255.255
                {1783627776, 1784676351}, // 106.80.0.0-106.95.255.255
                {2035023872, 2035154943}, // 121.76.0.0-121.77.255.255
                {2078801920, 2079064063}, // 123.232.0.0-123.235.255.255
                {-1950089216, -1948778497}, // 139.196.0.0-139.215.255.255
                {-1425539072, -1425014785}, // 171.8.0.0-171.15.255.255
                {-1236271104, -1235419137}, // 182.80.0.0-182.92.255.255
                {-770113536, -768606209}, // 210.25.0.0-210.47.255.255
                {-569376768, -564133889}, // 222.16.0.0-222.95.255.255
        };
        Random random = new Random();
        int index = random.nextInt(10);
        return num2ip(range[index][0] + new Random().nextInt(range[index][1] - range[index][0]));
    }
    // 将十进制转换成ip地址
    public static String num2ip(int ip) {
        int[] b = new int[4];
        String x;
        b[0] = (ip >> 24) & 0xff;
        b[1] = (ip >> 16) & 0xff;
        b[2] = (ip >> 8) & 0xff;
        b[3] = ip & 0xff;
        x = b[0] + "." + b[1] + "." + b[2] + "." + b[3];
        return x;
    }

    // 随机生成用户名
    static private String getRandomUserName() {
        Random random = new Random();
        int randomNum = random.nextInt(9989) + 10;
        return "user" + randomNum;
    }
}
