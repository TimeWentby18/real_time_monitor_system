import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;

public class MyRunnable implements Runnable {
    String insert;
    Connection conn;
    private Random random;
    private long orderID;
    private String userName;
    private String productID;
    private int number;
    private int price;

    public void setInsert(String insert) {
        this.insert = insert;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public void setOrderID(long orderID) {
        this.orderID = orderID;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public void run() {
        try {
            PreparedStatement state = conn.prepareStatement(insert);
            state.setLong(1, System.currentTimeMillis());
            state.setLong(2, orderID);
            double randomNum = random.nextDouble();
            if (randomNum < 0.75) {
                state.setString(3, "paid");
            } else if (randomNum < 0.85) {
                state.setString(3, "failed");
            } else {
                state.close();
                return;
            }
            state.setString(4, userName);
            state.setString(5, productID);
            state.setInt(6, number);
            state.setInt(7, price);
            state.executeUpdate();
            state.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
