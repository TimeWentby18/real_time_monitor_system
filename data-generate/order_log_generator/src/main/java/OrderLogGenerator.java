import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;

public class OrderLogGenerator {
    public static void main(String[] args) throws InterruptedException, ClassNotFoundException, SQLException {
        // 加载驱动程序
        Class.forName("com.mysql.jdbc.Driver");
        // 从命令行参数中获取数据库ip、用户名和密码
        String ip = args[0];
        String username = args[1];
        String pwd = args[2];
        // 创建连接
        String url = "jdbc:mysql://" + ip + ":3306/log_db?useUnicode=true&characterEncoding=utf-8&useSSL=false";
        Connection conn = DriverManager.getConnection(url, username, pwd);
        // 创建prepareStatement执行动态sql
        String insert = "insert into order_log(ts, order_id, type, user_name, product_id, number, price) values (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement state = conn.prepareStatement(insert);
        // 循环产生随机数据，并插入数据库
        // ts时间戳+orderID订单号+type订单类型(created/paid/failed)+userName用户名+productID商品ID+number数量+price交易金额
        long orderID = 1;
        String userName;
        String productID;
        int number;
        int price;
        Random random = new Random();
        Timer timer = new Timer(); // 用于异步生成订单日志
        while (true) {
            try {
                state.setLong(1, getTimeStamp());
                state.setLong(2, orderID);
                state.setString(3, "created");
                userName = getUserName();
                state.setString(4, userName);
                productID = getProductID();
                state.setString(5, productID);
                number = random.nextInt(100) + 1;
                state.setInt(6, number);
                price = random.nextInt(9999) + 1;
                state.setInt(7, price);
                state.executeUpdate();
                MyRunnable myRunnable = new MyRunnable();
                myRunnable.setInsert(insert);
                myRunnable.setConn(conn);
                myRunnable.setRandom(random);
                myRunnable.setOrderID(orderID);
                myRunnable.setUserName(userName);
                myRunnable.setProductID(productID);
                myRunnable.setNumber(number);
                myRunnable.setPrice(price);
                // 隔一段时间后生成对应的订单日志(支付成功或失败)
                timer.insert(myRunnable, random.nextInt(3500) + 1000);
            } catch (SQLException e) {
                e.printStackTrace();
                //关闭连接
                try {
                    state.close();
                    conn.close();
                    break;
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            orderID += 1L;
            Thread.sleep(200);
//            System.out.println("已提交sql请求！");
        }
    }

    // 获取当前时间戳
    static private Long getTimeStamp() {
        return System.currentTimeMillis();
    }

    // 随机生成用户名
    static private String getUserName() {
        Random random = new Random();
        int randomNum = random.nextInt(999) + 1;
        return "user" + randomNum;
    }

    // 随机生成商品ID
    static private String getProductID() {
        String[] arr = new String[]{"A", "B", "C", "D", "E", "F"};
        Random random = new Random();
        return arr[random.nextInt(6)];
    }

}
