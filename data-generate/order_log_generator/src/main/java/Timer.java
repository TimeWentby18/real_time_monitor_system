import java.util.concurrent.PriorityBlockingQueue;

public class Timer {
    // 保存待执行任务的优先级阻塞队列
    private final PriorityBlockingQueue<Task> queue = new PriorityBlockingQueue<>();

    // 描述任务的类
    private class Task implements Comparable<Task> {
        private final Runnable command;
        private final long time;

        public Task(Runnable command, long time) {
            this.command = command;
            this.time = time + System.currentTimeMillis();
        }

        //执行任务的方法
        public void run() {
            command.run();
        }

        @Override
        public int compareTo(Task o) {
            return (int) (this.time - o.time);
        }
    }

    // 创建扫描工具类
    private class Worker extends Thread {
        @Override
        public void run() {
            while (true) {
                try {
                    synchronized (this) {
                        Task task = queue.take();
                        long nowTime = System.currentTimeMillis();
                        if (task.time > nowTime) { // 把优先级最高的任务取出来，判断任务是否达到执行条件
                            queue.put(task);  // 如果没有就把任务放回去，让扫描等待当前时间和任务执行时间时间差后再次进行判断扫描
                            wait(task.time - nowTime);
                        } else {
                            task.run();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 构造函数
    public Timer() {
        Worker worker = new Worker();  // 当创建定时器类对象的时候就会启动worker扫描线程
        worker.start();
    }

    public void insert(Runnable runnable, long time) {  // 向定时器任务队列中添加待执行任务
        Task task = new Task(runnable, time);
        queue.put(task);
        // 此处调用notify是为了防止有新的任务加入之后，新的任务优先度高于之前队头元素优先度，
        // 所以此处调用notify唤醒扫描线程，重新判断优先度最高的任务是否达到执行条件
        synchronized (this) {
            notify();
        }
    }
}
