package com.example.monitor_backend.model;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * redis消息包装类
 */
@Component
@Data
public class RedisMessageWarp {
    private String topic;
    private String message;
}
