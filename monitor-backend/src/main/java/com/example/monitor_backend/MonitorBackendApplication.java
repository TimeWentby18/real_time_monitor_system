package com.example.monitor_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主入口类，启动整个SpringBoot项目
 */
@SpringBootApplication
public class MonitorBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonitorBackendApplication.class, args);
    }

}
