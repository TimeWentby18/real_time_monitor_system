package com.example.monitor_backend.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.example.monitor_backend.model.RedisMessageWarp;
import org.springframework.stereotype.Component;

/***
 * 从订阅的redis topic中接收到消息后的处理逻辑：
 * 将其包装成json对象并广播到所有的websocket连接
 */
@Component
public class RedisReceiver {

    public void receiveMessage(String message, String topic) {
//        System.out.println("received message!");
        RedisMessageWarp warp = new RedisMessageWarp();
        warp.setMessage(message);
        warp.setTopic(topic);
        WebSocket.sendAllMessage(JSON.toJSONString(warp, SerializerFeature.PrettyFormat));
    }
}
