package com.example.monitor_backend.controller;

import com.example.monitor_backend.common.WebSocket;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试广播发送消息
 */
@RestController
public class TestController {

    @RequestMapping("test")
    public String test() {
        String text="你们好！这是websocket广播发送的消息！";
        WebSocket.sendAllMessage(text);
        return text;
    }
}
