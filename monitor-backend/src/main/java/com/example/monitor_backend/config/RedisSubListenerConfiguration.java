package com.example.monitor_backend.config;

import com.example.monitor_backend.common.RedisReceiver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

/**
 * 配置redis发布订阅监听
 */
@Configuration
public class RedisSubListenerConfiguration {
    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory redisConnectionFactory,
                                            MessageListenerAdapter listenerAdapter) {
        // 监听redis相关topic以接收需要的消息
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(redisConnectionFactory);
        String[] topics = new String[] {
                "comment:alert:negative",
                "comment:alert:positive",
                "comment:bar:negative",
                "comment:bar:positive",
                "visit:line:pv",
                "visit:line:uv",
                "visit:line:ip",
                "visit:alert:pv",
                "visit:alert:uv",
                "visit:alert:ip",
                "order:value",
                "order:alert:sd",
                "order:alert:failed",
                "order:alert:overdue",
                "action:map:online",
                "action:alert:login",
                "action:alert:register"
        };
        for (String topic : topics) {
            container.addMessageListener(listenerAdapter, new PatternTopic(topic));
        }
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(RedisReceiver redisReceiver) {
        // 定义接收到消息后的处理方式
        return new MessageListenerAdapter(redisReceiver, "receiveMessage");
    }
}
