#!/bin/sh

export COMMENT=comment_log_generator-1.0-SNAPSHOT.jar
export VISIT=visit_log_generator-1.0-SNAPSHOT.jar
export ORDER=order_log_generator-1.0-SNAPSHOT.jar
export ACTION=action_log_generator-1.0-SNAPSHOT.jar
export RESET=data_reset-1.0-SNAPSHOT.jar

export IP=$2
export USERNAME=$3
export PWD=$4

case "$1" in

start)
        ## 检查参数是否填写
        if [[ -z $2 || -z $3 || -z $4 ]]
        then
            echo "参数不能为空，格式：./data-generator.sh start ip username password"
            exit 0
        else
            echo "database ip: $IP"
            echo "database username: $USERNAME"
            echo "database password: $PWD"
        fi
        ## 启动COMMENT
        echo "--------COMMENT 开始启动--------------"
        nohup java -jar $COMMENT $IP $USERNAME $PWD >/dev/null 2>&1 &
        COMMENT_pid=`sudo lsof $COMMENT | grep "mem" | awk '{print $2}'`
        until [ -n "$COMMENT_pid" ]
            do
              COMMENT_pid=`sudo lsof $COMMENT | grep "mem" | awk '{print $2}'`  
            done
        echo "COMMENT pid is $COMMENT_pid"
        echo "--------COMMENT 启动成功--------------"
		
        ## 启动VISIT
        echo "--------VISIT 开始启动--------------"
        nohup java -jar $VISIT $IP $USERNAME $PWD >/dev/null 2>&1 &
        VISIT_pid=`sudo lsof $VISIT | grep "mem" | awk '{print $2}'`
        until [ -n "$VISIT_pid" ]
            do
              VISIT_pid=`sudo lsof $VISIT | grep "mem" | awk '{print $2}'`  
            done
        echo "VISIT pid is $VISIT_pid"
        echo "--------VISIT 启动成功--------------"

        ## 启动ORDER
        echo "--------ORDER 开始启动--------------"
        nohup java -jar $ORDER $IP $USERNAME $PWD >/dev/null 2>&1 &
        ORDER_pid=`sudo lsof $ORDER | grep "mem" | awk '{print $2}'`
        until [ -n "$ORDER_pid" ]
            do
              ORDER_pid=`sudo lsof $ORDER | grep "mem" | awk '{print $2}'`  
            done
        echo "ORDER pid is $ORDER_pid"
        echo "--------ORDER 启动成功--------------"

        ## 启动ACTION
        echo "--------ACTION 开始启动--------------"
        nohup java -jar $ACTION $IP $USERNAME $PWD >/dev/null 2>&1 &
        ACTION_pid=`sudo lsof $ACTION | grep "mem" | awk '{print $2}'`
        until [ -n "$ACTION_pid" ]
            do
              ACTION_pid=`sudo lsof $ACTION | grep "mem" | awk '{print $2}'`  
            done
        echo "ACTION pid is $ACTION_pid"
        echo "--------ACTION 启动成功--------------"
		
        echo "===startAll success==="
        ;;
 
stop)
        P_ID=`sudo lsof $COMMENT | grep "mem" | awk '{print $2}'`
        if [ "$P_ID" == "" ]; then
            echo "===COMMENT process not exists or stop success==="
        else
            kill -9 $P_ID
            echo "COMMENT killed success"
        fi
		
		P_ID=`sudo lsof $VISIT | grep "mem" | awk '{print $2}'`
        if [ "$P_ID" == "" ]; then
            echo "===VISIT process not exists or stop success==="
        else
            kill -9 $P_ID
            echo "VISIT killed success"
        fi
		
		P_ID=`sudo lsof $ORDER | grep "mem" | awk '{print $2}'`
        if [ "$P_ID" == "" ]; then
            echo "===ORDER process not exists or stop success==="
        else
            kill -9 $P_ID
            echo "ORDER killed success"
        fi
		
		P_ID=`sudo lsof $ACTION | grep "mem" | awk '{print $2}'`
        if [ "$P_ID" == "" ]; then
            echo "===ACTION process not exists or stop success==="
        else
            kill -9 $P_ID
            echo "ACTION killed success"
        fi
		
        echo "===stopAll success==="
        ;;   

reset)
        echo "--------开始清除数据--------------"
        java -jar $RESET $IP $USERNAME $PWD >/dev/null 2>&1
        echo "--------数据清除成功--------------"

esac
exit 0